﻿using UnityEngine;
using System.Collections;

public class GroundBreackable : MonoBehaviour {
	public int collisonLimit = 2 ; 
	public int collisionCount ; 
	// Use this for initialization
	void Start () {
		collisionCount = 0; 
	}
	
	// Update is called once per frame
	void Update () {
		if(collisionCount>collisonLimit){

			// Breack the ground 
			transform.position = new Vector3(-1000, 0, 0) ; 
			Destroy(gameObject,5.0f);


		}
	}

	void OnCollisionEnter2D(Collision2D collision){
		if(collision.gameObject.tag =="Player"){
			collisionCount ++ ; 
		}
	}
}
