﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;




public class PlayerBehaviour2D : MonoBehaviour
{
    static public float jumpAngle;
    public bool isJumpHigher = false;
    public float accelerometreSensevity = 5.0f;
    static public float jumpRatioSwap;
    public float Velocity = 10.0f;
    static public float jumpVelocity;
    private bool isGrounded = false;
    private bool isJumping = false;
    public float jumpVelocityAngle = 45.0f;
    public int countCollision = 0;
    static public string direction;

    private Transform jumpObject;
    public float mass = 25.0f;
    private float jumpAngleDirection;
    private bool isBorderCollinding;
    private bool isCollidingWind;
    public float jumpRatio = 0.1f;
    public float jumpStopDuration = 1.0f;


    public float jumpObjectRotation;
    public int flipCount;





    public AudioClip starClip;
    public int starCount;
    static public bool isLevelFinished;

    public int metricScore;
    static public int finalMetricScore = 0;
    public int scorePath = 10;


    public float score = 0;

    Transform _transform;



    // Use this for initialization
    void Start()
    {
        jumpRatioSwap = jumpRatio;
        flipCount = 0;
        jumpObject = transform;
        isLevelFinished = false;
        isCollidingWind = false;
        starCount = 0;
        finalMetricScore = 0;
        direction = "None";
        _transform = transform;
    }



    void FixedUpdate()
    {

        if (countCollision > 0 && !isLevelFinished)
        {
            KeyBoardControl();
            RotationController();
            metricScore = starCount + Mathf.RoundToInt(score);
        }
        if (!isLevelFinished)
            JumpWithoutPhysics();

        score = _transform.position.x * scorePath + 5;
        if (metricScore > finalMetricScore && !isLevelFinished)
            finalMetricScore = metricScore;
    }



    //Collision Management

    void OnCollisionEnter2D(Collision2D collision)
    {

        if ((collision.gameObject.tag == "Ground"))
        {
            isGrounded = true;
            countCollision++;
            isJumping = true;
            jumpAngleDirection = _transform.eulerAngles.z;
            jumpAngle = Quaternion.Angle(_transform.rotation, jumpObject.rotation);


        }

        if ((collision.gameObject.name == "Border"))
        {
            isBorderCollinding = true;
        }

        if ((collision.gameObject.tag == "JumpHigher"))
        {

            isJumpHigher = true;
            countCollision++;
            isJumping = true;
        }
    }


    void OnCollisionExit2D(Collision2D collision)
    {
        if ((collision.gameObject.tag == "Ground"))
        {
            isGrounded = false;
            jumpRatioSwap = jumpRatio;
            jumpObject = collision.gameObject.transform;
            jumpObjectRotation = collision.gameObject.transform.eulerAngles.z;
        }

        if ((collision.gameObject.name == "Border"))
        {
            //isBorderCollinding = false;
            isLevelFinished = true;
        }

        if ((collision.gameObject.tag == "JumpHigher"))
        {
            isJumpHigher = false;
            //jumpAngle = Quaternion.Angle(transform.rotation, collision.gameObject.transform.rotation);
            jumpAngle = 90;

            jumpAngleDirection = _transform.eulerAngles.z;

            jumpRatioSwap = jumpRatio;
            jumpObject = collision.gameObject.transform;

            jumpVelocity = Velocity;

        }



    }

    /*	void OnTriggerEnter2D (Collider2D collision){
            if(collision.gameObject.tag=="Star"){
            //	starCount ++ ;
                audio.PlayOneShot(starClip, 0.7f);
                Destroy(collision.gameObject);
            }

            if((collision.gameObject.tag=="Finish")){
                isLevelFinished = true; 
            }
        }
    */


    //*Custom Functions	

    // Jump Without physics
    void JumpWithoutPhysics()
    {



        //Vertical Movemement

        if ((jumpVelocity >= -jumpStopDuration) && (jumpVelocity <= jumpStopDuration))
        {

            jumpVelocity = jumpVelocity - (Time.deltaTime * mass * 0.2f);


        }
        else
        {
            jumpVelocity = jumpVelocity - (Time.deltaTime * mass);
        }


        if (jumpVelocity > 0)
        {
            _transform.Translate(Vector3.up * Time.deltaTime * jumpVelocity, Space.World);
        }
        if ((jumpVelocity < 0))
        {
            _transform.Translate(Vector3.up * Time.deltaTime * jumpVelocity, Space.World);
            isJumping = false;
            if (isGrounded)
            {
                jumpVelocity = jumpVelocity + 1.7f;
                if (-jumpVelocity > Velocity)
                {
                    jumpVelocity = -jumpVelocity;
                }
                else
                {
                    jumpVelocity = Velocity;
                }

            }

            if (isJumpHigher)
            {
                _transform.rotation = Quaternion.Euler(0, 0, 270);
                if (-jumpVelocity > Velocity)
                {

                    jumpVelocity = Velocity + (Velocity * 0.0f);

                    direction = "right";

                }
                else
                {
                    jumpVelocity = -jumpVelocity + (Velocity * 0.5f);
                }


            }
        }

        //Horizontal Movement 

        if ((jumpAngleDirection > 180 && jumpAngleDirection < 359) && (jumpAngle < jumpVelocityAngle) && (jumpAngle > 40) && (jumpObjectRotation < 150.0f && jumpObjectRotation >= 0.0f))
        {

            _transform.Translate(Vector2.right * Time.deltaTime * jumpAngle * jumpRatioSwap, Space.World);


        }
        else if ((jumpAngleDirection > 0 && jumpAngleDirection < 180) && (jumpAngle < jumpVelocityAngle) && (jumpAngle > 40) && ((jumpObjectRotation < 359.0f && jumpObjectRotation > 360 - 150.0f) || (jumpObjectRotation < 2.0f && jumpObjectRotation >= 0.0f)))
        {

            _transform.Translate(-Vector2.right * Time.deltaTime * jumpAngle * jumpRatioSwap, Space.World);

        }


        if ((jumpAngleDirection > 180 && jumpAngleDirection < 359) && (jumpAngle <= 40) && (jumpAngle > 0) && (jumpObjectRotation < 150.0f && jumpObjectRotation >= 0.0f))
        {

            _transform.Translate(Vector2.right * Time.deltaTime * jumpAngle * jumpRatioSwap * 0.3F, Space.World);




        }
        else if ((jumpAngleDirection > 0 && jumpAngleDirection < 180) && (jumpAngle <= 40) && (jumpAngle > 0) && ((jumpObjectRotation < 359.0f && jumpObjectRotation > 360 - 150.0f) || (jumpObjectRotation < 2.0f && jumpObjectRotation >= 0.0f)))
        {

            _transform.Translate(-Vector2.right * Time.deltaTime * jumpAngle * jumpRatioSwap * 0.3F, Space.World);



        }



    }


    //Rotation Controller
    void RotationController()
    {
        if (direction == "right")
        {

            _transform.Rotate(0, 0, -accelerometreSensevity);
        }

        if (direction == "left")
        {
            _transform.Rotate(0, 0, accelerometreSensevity);
        }


        if ((jumpAngle == 0))
        {

        }
    }


    //Input Controller
    void KeyBoardControl()
    {
        if (Input.GetAxis("Horizontal") < 0)
        {
            _transform.Rotate(0, 0, 4);
            direction = "left";

        }
        else if (Input.GetAxis("Horizontal") > 0)
        {
            _transform.Rotate(0, 0, -4);
            direction = "right";
        }


    }






    void OnGUI()
    {
        float res = Screen.width;
        float resH = Screen.height;


        if (GUI.RepeatButton(new Rect(0, 0, Screen.width * 0.5f, Screen.height), "", GUIStyle.none))
        {
            _transform.Rotate(0, 0, 4);
            direction = "left";

            Debug.Log("Invisble left button pressed");
        }


        if (GUI.RepeatButton(new Rect(Screen.width - Screen.width * 0.5f, 0, Screen.width, Screen.height), "", GUIStyle.none))
        {
            _transform.Rotate(0, 0, -4);
            direction = "right";

            Debug.Log("Invisble right button pressed");
        }



    }








    //Getter functions
    public void replay()
    {
        Application.LoadLevel(Application.loadedLevel);
    }


}

