﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class GroundQuadraTranslating : MonoBehaviour {
	public Transform Marker_1;
    public Transform Marker_2;
    public Transform Marker_3;
    public Transform Marker_4;
    private Transform newPosition;
    public float speed = 5.0F;
    Sequence seq;
    Transform _transform;
	// Use this for initialization
	void Start () {
		
		//transform.position = Marker_1.position;
        _transform = transform;

        seq = new Sequence(new SequenceParms().Loops(-1));



        seq.Append(HOTween.To(
            _transform,
            speed,
            "position",
            Marker_2.position,
            false,
            EaseType.Linear,
            0
        ));

        seq.Append(HOTween.To(
            _transform,
            speed,
            "position",
            Marker_3.position,
            false,
            EaseType.Linear,
            0
        ));

        seq.Append(HOTween.To(
            _transform,
            speed,
            "position",
            Marker_4.position,
            false,
            EaseType.Linear,
            0
        ));

        seq.Append(HOTween.To(
        _transform,
        speed,
        "position",
        Marker_1.position,
        false,
        EaseType.Linear,
        0
        ));

        seq.Play();
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        /*

        if (_transform.position == Marker_1.position)
        {
            newPosition = Marker_2;
		}

        if (_transform.position == Marker_2.position)
        {
            newPosition = Marker_3;
        }

        if (_transform.position == Marker_3.position)
        {
            newPosition = Marker_4;
        }

        if (_transform.position == Marker_4.position)
        {
            newPosition = Marker_1;
        }
	
       */

      /* 
       if(Input.GetKeyDown(KeyCode.Z)){
            newPosition = Marker_1;
		}
        
    	if(Input.GetKeyDown(KeyCode.D)){
            newPosition = Marker_2;
        }

        if(Input.GetKeyDown(KeyCode.S)){
            newPosition = Marker_3;
        }

        if(Input.GetKeyDown(KeyCode.Q)){
            newPosition = Marker_4;
        }
        */
   // transform.position = Vector3.Lerp(transform.position,newPosition.position, speed * Time.time);
       
        
       
	}
}
