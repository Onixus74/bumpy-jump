﻿using UnityEngine;
using System.Collections;

public class PushPlayer : MonoBehaviour {
	public Rigidbody player; 
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter(Collision collision){
		if(collision.gameObject.tag=="Player"){
			player.AddRelativeForce(Vector3.up * Time.deltaTime);
		}
	}
}
