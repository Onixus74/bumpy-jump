﻿using UnityEngine;
using System.Collections.Generic;

public class GroundManager : MonoBehaviour {

    public Transform prefab;
    public Transform prefabStar;
    public Vector3 startPosition;
    public int numberOfObjects;
    public float recycleOffset;
    public Vector3 minSize, maxSize, minGap, maxGap;

    public float previousRandomX = 3.0f;
    public float previousRandomY = -3.5f;


    private Vector3 nextPosition;
    public float minY, maxY;
    private Queue<Transform> objectQueue;


    public int element_1;
    public int element_2;
    public int element_3;
    public int element_4;
    public int element_5;



    public Transform breackable;
    public Transform jumpHigher;
    public Transform linear;
    public Transform quadra;
    public Transform rotative;
    public Transform elementNull;
    public Transform previousElement;
    public Transform prevPreviousElement;

    public Transform elementFire;

    float nullElementVariance = 1.0f; 
    public Transform star; 
    

	// Use this for initialization
	void Start () {
        float x, y, width, height;
        objectQueue = new Queue<Transform>(numberOfObjects);

        Transform element=null;
        previousElement = null;
        prevPreviousElement = null;


        element_1 = RandomElementRange(8,11);
        element_2 = RandomElementRange(18,21);
        element_3 = RandomElementRange(28,31);
        element_4 = RandomElementRange(38,41);
        element_5 = RandomElementRange(48,51);

        for (int i = 0; i < numberOfObjects; i++)
        {   
            /*
            objectQueue.Enqueue((Transform)Instantiate(
                prefab,Randomize(i,5.0f,0.5f,5.0f,-3.5f), Quaternion.identity));
             */
            //x = RandomizeFloat(5.0f,5.0f);
            x = 4.0f;
            y = RandomizeFloat(-3.5f, previousRandomY+1.5f );
            if (y > 3.5f)
                y =  2.0f;
            /*objectQueue.Enqueue((Transform)Instantiate(
                prefab, new Vector3(i*x,y,0), Quaternion.identity));*/
          
                if (i >= 0 && i <= numberOfObjects * 0.1)
                {
                    element = RandomElement(2, 5);
                }
                else
                    if (i > numberOfObjects * 0.1 && i <= numberOfObjects * 0.2 * 2)
                    {
                        element = RandomElement(40, 5);
                    }
                    else
                        if (i > numberOfObjects * 0.2 * 2 && i <= numberOfObjects * 0.2 * 3)
                        {
                            element = RandomElement(50, 4);
                        }
                        else
                            if (i > numberOfObjects * 0.2 * 3 && i <= numberOfObjects * 0.2 * 4)
                            {
                                element = RandomElement(60, 3);
                            }
                            else
                                if (i > numberOfObjects * 0.2 * 4 && i <= numberOfObjects * 0.2 * 5)
                                {
                                    element = RandomElement(70, 2);
                                }
                                else
                                {
                                    element = RandomElement(100, 1);
                                }

               // element = RandomElement(2, 1);
              
                    int dice = Random.Range(0, numberOfObjects);
                    dice = Mathf.RoundToInt(dice);
                    if (dice < i)
                    {
                        element = elementNull;
                    }


                    dice = Random.Range(0, numberOfObjects);
                    dice = Mathf.RoundToInt(dice);
                    if (dice < i && previousElement!= jumpHigher && previousElement != linear)
                    {
                        Instantiate(
                                 elementFire, new Vector3(i * x  + 2, -3.5f, 0), Quaternion.identity);
                    }

                        
                    
                
            
                if (element == previousElement)
                {
                    element = prefab;
                }
                
                if (previousElement == elementNull)
                {
                    element = prefab;
                    y = -3.5f;
                    //Debug.Log("Null Element spoted");
                   // nullElementVariance = numberOfObjects*0.01f -( i/numberOfObjects )  ; 
                    nullElementVariance = 0; 
                }
                else
                {
                    nullElementVariance = 0; 
                }

                

                if (element == elementNull)
                {
                   // prefabStar = elementNull;
                    x = 4.0f;
                }
                else
                {
                    prefabStar = star; 
                }

                if (prevPreviousElement == rotative)
                {
                    Debug.Log("Jump higher spotted");
                    element = prefab;
                    y = -3.5f;
                }


                if (element == jumpHigher)
                {
                    y = Random.Range(previousRandomY, -2.9f);
                }

              //  
           
            
            objectQueue.Enqueue((Transform)Instantiate(
           element, new Vector3(i * x - nullElementVariance, y, 0), Quaternion.identity));




            //Random Element 
          /*  if (i == element_1)
            {
                element = RandomElement();
                objectQueue.Enqueue((Transform)Instantiate(
               element, new Vector3(i * x, y, 0), Quaternion.identity));
            }
            else
            {
                objectQueue.Enqueue((Transform)Instantiate(
                prefab, new Vector3(i * x, y, 0), Quaternion.identity));
            }
*/
            //Instantiate Star
          /*  objectQueue.Enqueue((Transform)Instantiate(
                prefabStar, new Vector3(i * x - nullElementVariance, y +1, 0), Quaternion.identity));
            */

            previousRandomY = y;
            previousRandomX = x; 
           
           /* if (i < 1)
                prevPreviousElement = element;
            else*/
                prevPreviousElement = previousElement;


            previousElement = element;
           

            

        }
        enabled = false;
	}
	// Update is called once per frame
	void Update () {
	}

    Vector3 Randomize(int index, float x , float y , float width , float height)
    {
        return new Vector3(index * Random.Range(x,width), Random.Range(y,height), 0);
    }
    
    float RandomizeFloat(float x1, float x2)
    {
        return Random.Range(x1, x2);
    }

    int RandomElementRange(int index_1, int index_2)
    {
        int index;
        index = Random.Range(index_1, index_2);
        index = Mathf.RoundToInt(index);
        return index;
    }

    Transform RandomElement(int pourcentage , int plateform)
    {
        int dice;
        Transform element;

        if (pourcentage == 100)
        {
            dice = Random.Range(6,20 );
        }
        else
        {
            

            switch(plateform){
                case 1 :
                    dice = Random.Range(6, (100 / pourcentage));
                    break;
                case 2:
                    dice = Random.Range(4, 2*(100 / pourcentage));
                    break;
                case 3:
                    dice = Random.Range(3, 3 * (100 / pourcentage));
                    break;
                case 4:
                    dice = Random.Range(2, 4 * (100 / pourcentage));
                    break;
                case 5:
                    dice = Random.Range(1, 5 * (100 / pourcentage));
                    break;
                default:
                    dice = Random.Range(6, 5 * (100 / pourcentage));
                    break;
            }
        }

        
        dice = Mathf.RoundToInt(dice);
        switch (dice)
        {
            case 1:
                {
                    element= breackable;
                }
            break;
            case 2:
                {
                    element = jumpHigher;
                }
            break;
            case 3:
                {
                    element = linear;
                }
            break;
            case 4:
                {
                    element = quadra;
                }
            break;
            case 5:
                {
                    element = rotative;
                }
            break;
            default :
            element = prefab;
            break;

        }

        return element;
    }

    
}
