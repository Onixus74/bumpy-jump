﻿using UnityEngine;
using System.Collections;

public class PlayerBehaviour_Physics_Enabled : MonoBehaviour {
	public float jumpVelocity = 100;

	public bool isGrounded= false ;
	public float jumpAngle ; 
	public int countCollision = 0 ;
	public string direction;


	
	// Use this for initialization
	void Start() {
		gameObject.rigidbody.useGravity = true;
	}
	
	// Update is called once per frame
	void FixedUpdate () {


		
		if(isGrounded){
			Debug.Log("Collision"+countCollision);
			Jump();
		}

		

//----------------Inputs ------------------------------


		KeyBoardControl();


//-------------END Inputs-------------------------------


		if(direction=="right"){
		
		transform.Rotate(0, 0, -1 );
		}

		 if(direction=="left"){
				transform.Rotate(0, 0, 1 );
		}

		 if((jumpAngle == 0)){
				transform.Rotate(0, 0, 0 );
		}
//------------------------End Update----------------------------

	}


	//Collision Management
	
	void OnCollisionEnter(Collision collision){
		if((collision.gameObject.tag=="Ground")){
			isGrounded=true;
			countCollision++;
			jumpAngle = Quaternion.Angle(transform.rotation, collision.gameObject.transform.rotation);
			
		}
	}

	void OnCollisionStay(Collision collision){
		if((collision.gameObject.tag=="Ground")){
			//isGrounded=true;
			//countCollision++;
			//jumpAngle = Quaternion.Angle(transform.rotation, collision.gameObject.transform.rotation);
			
		}
	}

	void OnCollisionExit(Collision collision){
		if((collision.gameObject.tag=="Ground")){
			isGrounded=false;
			//countCollision=0;

		}
	}

	//*Custom Functions

	//**Jump With physics
	void Jump(){
		if(jumpAngle<45){
		rigidbody.AddRelativeForce(Vector3.up * (jumpVelocity +jumpAngle * 2));
	
	}
		if((jumpAngle>45)&&(jumpAngle<75))
		rigidbody.AddRelativeForce(Vector3.up * (jumpVelocity-20));

		
	}

	//**Input Controller
	void KeyBoardControl(){
		if(Input.GetAxis("Horizontal")<0){
			transform.Rotate(0, 0, 3 );
			direction="left";
		}
		if (Input.GetAxis("Horizontal")>0){
			transform.Rotate(0, 0, -3 );
			direction="right";
		}


	}

}
