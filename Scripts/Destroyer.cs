﻿using UnityEngine;
using System.Collections;

public class Destroyer : MonoBehaviour {

    public GameObject player;
    Transform _transform;
	// Use this for initialization
	void Start () {
        _transform = transform;
        player = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
        if (player.transform.position.x > _transform.position.x + 10)
        {
            Debug.Log("Desctruction");
            Destroy(gameObject);
        }
	}
}
