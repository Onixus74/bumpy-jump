﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BestScore : MonoBehaviour {
    Text bscore;
	// Use this for initialization
	void Start () {
        bscore = GetComponent<Text>();
        bscore.text = "Best score : "+PlayerPrefs.GetInt("score");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
