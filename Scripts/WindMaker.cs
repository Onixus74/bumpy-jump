﻿using UnityEngine;
using System.Collections;

public class WindMaker : MonoBehaviour {
    public Transform player;
    public Rigidbody2D playerRigid;
    private int forceValue;
    public int forceMax = 500;
    Vector2 direction;
    public int forceAdd = 50;
	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        playerRigid = GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody2D>();
        forceValue = 0;
        direction = transform.up;
	}


    void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player"){


            forceValue = 0;
        }
        
    }

    void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {

            if(forceValue<forceMax){
                forceValue+=forceAdd;
            }
            
             playerRigid.AddForce(direction.normalized * forceValue /*,ForceMode2D.Force*/);
        }

    }
}
