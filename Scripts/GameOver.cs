﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameOver : MonoBehaviour {

    Text score;
    int bScore;
    Transform _transform;
	// Use this for initialization
	void Start () {
        _transform = transform;
        score = _transform.GetChild(0).GetChild(2).GetComponent<Text>();
        if (PlayerBehaviour2D.finalMetricScore > PlayerPrefs.GetInt("score"))
        {
            PlayerPrefs.SetInt("score", PlayerBehaviour2D.finalMetricScore);
            score.text = "" + PlayerBehaviour2D.finalMetricScore;
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (PlayerBehaviour2D.isLevelFinished)
        {
            _transform.parent.GetChild(0).gameObject.SetActive(false);
            _transform.GetChild(0).gameObject.SetActive(true);
            score.text = "" + PlayerBehaviour2D.finalMetricScore;

            
        }
	}
}
