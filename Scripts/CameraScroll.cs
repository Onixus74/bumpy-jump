﻿using UnityEngine;
using System.Collections;

public class CameraScroll : MonoBehaviour {
    public Transform target;
    Transform _transform;
	// Use this for initialization
	void Start () {
        target = GameObject.FindGameObjectWithTag("Player").transform;
        _transform = transform;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        _transform.position = new Vector3(target.position.x + 5, _transform.position.y, _transform.position.z);
	}
}
