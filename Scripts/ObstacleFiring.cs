﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class ObstacleFiring : MonoBehaviour {
	public Transform startMarker;
    public Transform endMarker;
    private Transform newPosition;
    Transform _transform;
    public float speed = 5.0F;
    Sequence seq;
	// Use this for initialization
	void Start () {
        seq = new Sequence(new SequenceParms().Loops(-1));
        _transform = transform;
		//newPosition = startMarker;
		//transform.position = startMarker.position;


        seq.Append(HOTween.To(
            _transform,
            speed,
            "position",
            endMarker.position,
            false,
            EaseType.EaseOutQuad,
            0
        ));

        seq.Append(HOTween.To(
            _transform,
            0.1f,
            "rotation",
            new Vector3(0,0,180)

        ));
        seq.Append(HOTween.To(
           _transform,
           speed,
           "position",
           startMarker.position,
           false,
           EaseType.EaseInQuad,
           0
       ));
        seq.Play();
	}

    void OnCollisionEnter2D(Collision2D collision){
        if(collision.gameObject.tag=="Player"){
            PlayerBehaviour2D.isLevelFinished=true;
        }
    }
	
	// Update is called once per frame
	void Update () {
		
		/*transform.position = Vector3.Lerp(transform.position,newPosition.position, speed * Time.deltaTime);

		 if(transform.position == endMarker.position)
            newPosition = startMarker;
        if(transform.position == startMarker.position)
            newPosition = endMarker;

		*/
	}
}
