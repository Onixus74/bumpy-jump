﻿using UnityEngine;
using System.Collections;

public class WindTimer : MonoBehaviour {
    public float onTimer;
    private float offTimer;
    public float phasingTimerON = 9.0f;
    public float phasingTimerOFF = 9.0f;
    public bool windON ;
    public float timer;

	// Use this for initialization
	void Start () {
        onTimer = Time.time + phasingTimerON;
        timer = onTimer;
        windON = true;
	}
	
	// Update is called once per frame
	void Update () {
        timer = onTimer - Time.time;

        if (timer < 0 && windON) {
            onTimer = Time.time + phasingTimerOFF;
            timer = onTimer;
            windON = false;
            
            transform.parent.GetChild(1).GetComponent<ParticleSystem>().startSpeed = 0;
            Debug.Log("Time Off");

        }
        if (timer < 0 && windON == false)
        {
            onTimer = Time.time + phasingTimerON;
            timer = onTimer;
            windON = true;
            
            transform.parent.GetChild(1).GetComponent<ParticleSystem>().startSpeed = 4.7f;
            Debug.Log("Time On");
        }

        if (windON == true)
        {
            transform.GetComponent<WindMaker>().enabled = true;
            transform.GetComponent<CircleCollider2D>().enabled = true;
        }
        else
        {
            transform.GetComponent<WindMaker>().enabled = false;
            transform.GetComponent<CircleCollider2D>().enabled = false;
        }

            
	}
}
