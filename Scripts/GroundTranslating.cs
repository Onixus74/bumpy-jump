﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class GroundTranslating : MonoBehaviour {
	public Transform startMarker;
    public Transform endMarker;
    private Transform newPosition;
    Transform _transform;
    public float speed = 5.0F;
    Sequence seq;
	// Use this for initialization
	void Start () {
        seq = new Sequence(new SequenceParms().Loops(-1));
        _transform = transform;
		//newPosition = startMarker;
		//transform.position = startMarker.position;


        seq.Append(HOTween.To(
            _transform,
            speed,
            "position",
            endMarker.position,
            false,
            EaseType.Linear,
            0
        ));
        seq.Append(HOTween.To(
           _transform,
           speed,
           "position",
           startMarker.position,
           false,
           EaseType.Linear,
           0
       ));
        seq.Play();
	}
	
	// Update is called once per frame
	void Update () {
		
		/*transform.position = Vector3.Lerp(transform.position,newPosition.position, speed * Time.deltaTime);

		 if(transform.position == endMarker.position)
            newPosition = startMarker;
        if(transform.position == startMarker.position)
            newPosition = endMarker;

		*/
	}
}
