using Holoville.HOTween;
using UnityEngine;

public class AnimationCurveEase : MonoBehaviour
{
	// Tween targets
	public Transform tweenTarget;
	public Transform tweenSpringTarget;

	// These custom eases will be showed in the inspector,
	// so that they can be easily customized
	public AnimationCurve customEase;
	public AnimationCurve customEaseSpring;


	void Start()
	{
		// Create a sequence with a tween that will be repeated infinitely,
		// and will pause for 1 second at each loop completion.
		// We're using a Sequence just to implement the pause between
		// loops, otherwise we could simply create a regular Tweener.
		Sequence seq = new Sequence(new SequenceParms().Loops(-1));
		seq.Append(HOTween.To(tweenTarget, 2, new TweenParms()
			.Prop("rotation", new Vector3(0, 360, 0), true)
			.Ease(customEase)
		));
		seq.AppendInterval(1);
		seq.Play();

		// Now we create another Sequence, but this time it will be divided
		// in 2 Tweeners: one which will jump to a rotation,
		// and the other which will "spring back" to the original one.
		Sequence springSeq = new Sequence(new SequenceParms().Loops(-1));
		springSeq.Append(HOTween.To(tweenSpringTarget, 0.25f, new TweenParms()
			.Prop("rotation", new Vector3(0, 180, 0), true)
			.Ease(EaseType.EaseOutQuad)
		));
		springSeq.Append(HOTween.To(tweenSpringTarget, 1.75f, new TweenParms()
			.Prop("rotation", new Vector3(0, -180, 0), true)
			.Ease(customEaseSpring)
		));
		
		springSeq.Play();
	}
}